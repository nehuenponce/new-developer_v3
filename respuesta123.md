# Pruebarebajatuscuentas.com - Nehuen Ponce

1. La ultima vez que usé RTFM y LMGTFY:
siempre trato de buscar y leer la documentación. Siempre que tengo una duda, busco la respuesta en
la documentación oficial o todo caso, en Google.
Pero nunca use LMGTFY,ya que conozco y se que se usa como una forma sarcástica de decirle a alguien que busque en Google.
Yo pienso que lo correcto es tratar de acompañar a las personas a encontrar la respuesta.

Sistema Operativo: Windows
Lenguajes de Programacion Conocidos: Python (Django), HTML5, CSS3 (Bootstrap, MUI y SASS),
JavaScript (React, Node.js, Express.js, MongoDB).

2. Principios de Sistemas CI:
   La CI (Integracion Continua) es una práctica recomendada de DevOps y es la etapa del ciclo de vida de DevOps en que los desarrolladores deben registrar el código en su repositorio de código compartido, a menudo, múltiples veces al día.
   Lo ideal es que, cada vez que esto sucede, una herramienta de compilación automatizada se encargue de verificar el registro o la rama para garantizar que no haya errores y que esté listo para ingresar a producción.
   El principal beneficio de esta practica es que los problemas generalmente se detectan en una etapa inicial, antes de que estos se conviertan en problemas de mayor gravedad.
   La práctica de CI implica integrar pequeños subconjuntos de cambios en un período más corto de tiempo,
   en lugar de integrar actualizaciones importantes que toman más tiempo con menor frecuencia.
   Automatizar los flujos de trabajo para probar, fusionar y verificar los cambios en un repositorio compartido significa que los equipos pueden entregar un código más limpio a un ritmo mucho mas rápido.
   Un código más limpio significa una validación más rápida, versiones más limpias y un canal de desarrollo más eficiente que es más fácil de modificar y escalar con el tiempo.

   Herramientas de CI: Github Actions y Jenkins (Son los que he utilizado)


3. Ejercicios:
 1- Realiza una función llamada area_rectangulo() que devuelva el área del rectángulo a partir de una base y una altura. Calcula el área de un rectángulo de 15 de base y 10 de altura

Respuesta:
   
   def area_rectangulo(base, altura):
   return base \* altura

area_rectangulo(15, 10)

2- Realiza una función llamada area_circulo() que devuelva el área de un círculo a partir de un radio. Calcula el área de un círculo de 5 de radio

Respuesta:

from math import pi
def area_circulo(radio):
return pi \* radio\*\*2
area_circulo(5)

3- Realiza una función llamada relacion() que a partir de dos números cumpla lo siguiente:

a- Si el primer número es mayor que el segundo, debe devolver 1.
b- Si el primer número es menor que el segundo, debe devolver -1.
c- Si ambos números son iguales, debe devolver un 0.

Comprueba la relación entre los números: '5 y 10', '10 y 5' y '5 y 5'

Respuesta:

def relacion(num1, num2):
if num1 > num2:
return 1
elif num1 < num2:
return -1
return 0

print(relacion(5, 10), relacion(10, 5), relacion(5, 5))

4- Realiza una función llamada intermedio() que a partir de dos números, devuelva su punto intermedio.

Respuesta:

def intermedio(num1, num2):
return (num1 + num2) / 2

intermedio(-12, 24)

5- Realizá una función llamada recortar() que reciba tres parámetros. El primero es el número a recortar, el segundo es el límite inferior y el tercero el límite superior. La función tendrá que cumplir lo siguiente:

a- Devolver el límite inferior si el número es menor que éste
b -Devolver el límite superior si el número es mayor que éste.
c- Devolver el número sin cambios si no se supera ningún límite.

Comprueba el resultado de recortar 15 entre los límites 0 y 10

Respuesta:

def recortar(num, lim_inf, lim_sup):
return max(min(num, lim_sup), lim_inf)

recortar(-2, 0, 10)

6- Realiza una función separar() que tome una lista de números enteros y devuelva dos listas ordenadas. La primera con los números pares, y la segunda con los números impares.

Respuesta:

num = [-12, 84, 13, 20, -33, 101, 9]
def separar(\*args):
lista = sorted(args)
pares = []
impares = []
for arg in lista:
if arg % 2 == 0:
pares.append(arg)
else:
impares.append(arg)

    return pares, impares

pares, impares = separar(\*num)

print(pares)  
print(impares)
