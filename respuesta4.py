El pseudocodigo:
Cree este pseudocodigo y resolvi el problema utilizando un enfoque basado en el método de los dos punteros:

Función encontrarParCoincidente(numeros, suma):
izquierda = 0
derecha = longitud(numeros) - 1

    mientras izquierda < derecha:
        sumaActual = numeros[izquierda] + numeros[derecha]

        si sumaActual es igual a suma:
            retornar (numeros[izquierda], numeros[derecha])
        sino si sumaActual es menor que suma:
            incrementar izquierda en 1
        sino:
            decrementar derecha en 1

    return "No se encontró ningún par coincidente"

numeros = [2, 3, 6, 7]
suma = 9
resultado = encontrarParCoincidente(numeros, suma)
imprimir(resultado)

"Este algoritmo posee una complejidad temporal de O(n), donde n es el número de elementos en la colección. Utiliza dos punteros para explorar la colección y encontrar el par coincidente que suma el valor dado."